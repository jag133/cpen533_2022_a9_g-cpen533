# Group ID: G-CPEN533

# Verification Code: 1F4AF96F01FE5F4130CDA9C92718E4F5 

# Used Run Command: java -Xmx64m -jar A9.jar [ID] [Path to the server configuration file]
E.g. java -Xmx64m -Xms64m -jar A9.jar 13 /home/ubuntu/workspace/A6/server.file
	

# Brief Description: 

# Argument Description: 	

ID: Mandatory hash ID of each server instance.

Server Configuration File Path: Mandatory file path of the server configuration file.

# Proof of server honor immediate crash/termination upon receiving the shutdown request:

Filename: KeyValueMessageResponseTask.java

Line number: 190

# Design:

For balancing the loading among the server instances, we add an argument to assign predefined hash ID in server configuration file for a server instance.

For instance, "java -Xmx64m -jar A7.jar 25" means that this server instance is running with IP 10.0.0.195 and port 1213 and responsible for the range 251 - 25 of the hash ring.

Below is a sample, the actual implementation got 20 nodes.

25:10.0.0.195:1213

50:10.0.0.195:1214

75:10.0.0.195:1215

100:10.0.0.195:1216

125:10.0.0.195:1217

150:10.0.0.195:1218

175:10.0.0.195:1219

200:10.0.0.195:1220

225:10.0.0.195:1221

250:10.0.0.195:1222

# Test cases:

- We add detail logs to show when and which server instance handled which command with routing and command details, the changes of memory usage in KV store, cache, free memory left, used memory, etc, for identifying the root cause of routing or memory problems.

- The routing logs show the hash code of the key of command PUT, GET or REM and the mapped server ID. We trace if the command can be routed correctly to the right server.

- The command logs show the commands handled by different server instances over the time with details of key, value and version so that we can trace if expected result has happened and where went wrong if not.

# Points to note:
1. We use 87203410 as the submission student ID for the evaluation client		
2. The A9.jar and A9.log files are under the base directory "CPEN533_2022_A9_G-CPEN533".		

---
