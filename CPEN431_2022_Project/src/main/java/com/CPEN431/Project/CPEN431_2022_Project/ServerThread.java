package com.CPEN431.Project.CPEN431_2022_Project;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.CPEN431.Project.CPEN431_2022_Project.RequestReplyProtocolHandle.ReceivedRequest;

import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.NodeStatus;

public class ServerThread extends Thread {
  
 
   private int inPortNumber = 4445;
   private int serverId;
   protected boolean isRunning;
   protected KeyValueHandle valueHandle ;
   protected RequestReplyProtocolHandle protocolHandle;
   protected RequestReplyProtocolHandle internalProtocolHandle;
   protected EpicdemicServer epicedmicServer;
   protected MessageHandle msgHandle;
	public ServerThread(int sId, NStatus nstatus,int mlimit) throws IOException {
		 this("ServerThread",sId,nstatus,mlimit);
	}
		 
    public ServerThread(String name,int sId, NStatus ns,int mlimit) throws IOException {
        super(name); 
        serverId = sId;
        
       
    	try {
			epicedmicServer  = new EpicdemicServer(serverId, ns);
			epicedmicServer.start();
			
		
			NodeStatus node = epicedmicServer.getNodeStatus(sId);
			if ( node != null )
				this.inPortNumber = node.getPort();
			
			msgHandle = new MessageHandle(inPortNumber,14000);
		    protocolHandle = new RequestReplyProtocolHandle(inPortNumber,msgHandle);
		    internalProtocolHandle = new RequestReplyProtocolHandle(epicedmicServer.getInternalReplyPort(),msgHandle);
		       
			
			valueHandle = new KeyValueHandle(mlimit, epicedmicServer,internalProtocolHandle,msgHandle);
		}catch (Exception exp) {
			System.out.println("error in main" + exp.getMessage());
			 
		}
        isRunning = true;
        
        
	
//        Timer cleanTimer = new Timer ();
//        TimerTask cleanTask = new TimerTask () {
//            @Override
//            public void run () {
//            	try{
//            		KeyValueMessageResponseTask.cleanLegacyCache();
//            	}catch (Exception exp) {
//            		exp.printStackTrace();
//            	}
//            }
//        };
//
//        cleanTimer.scheduleAtFixedRate(cleanTask , 0l, App.gcTime);
		Thread t = new Thread(new CacheCleaner());
		t.start();
    }
    
    public static int getPID() {
    	int result =0;
        String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            
             result =  Integer.parseInt(processName.split("@")[0]);
        }
        return result;
    }	 
    public void run() {
        Builder responseBuilder = KVResponse.newBuilder();
        Msg repliedMessage = null;
        ReceivedRequest request = null;
        
//    	ExecutorService pool = Executors.newFixedThreadPool(App.numberOfThread); 
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(App.numberOfTasks);
        ThreadPoolExecutor pool = new ThreadPoolExecutor(1, App.numberOfThread, 500L, TimeUnit.MILLISECONDS, queue);    	
//        ExecutorService executor = Executors.newCachedThreadPool();

        // Cast the object to its class type
//        ThreadPoolExecutor pool = (ThreadPoolExecutor) executor;
        
        while (isRunning) {
            try {
                
                
             
                request = protocolHandle.receive(0);
                KeyValueMessageResponseTask task= new KeyValueMessageResponseTask( valueHandle,protocolHandle,msgHandle,request,epicedmicServer );
//                Thread t = new Thread(task);
//                t.start();
//                executor.submit(task);
                pool.execute(task);
                //task.run();
//    	        System.out.println("Queue size: " + queue.size() + " Remaining Capacity: " + queue.remainingCapacity());
            } catch (java.util.concurrent.RejectedExecutionException e) {
//                e.printStackTrace();
    			responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
             	repliedMessage = msgHandle.getMessage(responseBuilder.build().toByteArray(),request.message.getMessageID().toByteArray());
             
        		protocolHandle.send(request.requestIPAddress, request.requestPortNumber, repliedMessage);
//    	        System.out.println("Queue size: " + queue.size() + " Remaining Capacity: " + queue.remainingCapacity());
//    	        System.out.println("Overload due to Thread Queue.");
            } catch (IOException e) {
                e.printStackTrace();
            	
            }
            // wait for termination        
        }
        pool.shutdown();
//        System.out.println("Core threads: " + pool.getCorePoolSize());
//        System.out.println("Largest executions: "
//           + pool.getLargestPoolSize());
//        System.out.println("Maximum allowed threads: "
//           + pool.getMaximumPoolSize());
//        System.out.println("Current threads in pool: "
//           + pool.getPoolSize());
//        System.out.println("Currently executing threads: "
//           + pool.getActiveCount());
//        System.out.println("Total number of threads(ever scheduled): "
//           + pool.getTaskCount());
//        executor.shutdown();
//        protocolHandle.close();
        System.exit(0);
       
    }
		 
    
}
