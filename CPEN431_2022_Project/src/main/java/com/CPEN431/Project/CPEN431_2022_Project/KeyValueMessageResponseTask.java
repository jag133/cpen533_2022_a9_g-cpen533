package com.CPEN431.Project.CPEN431_2022_Project;

import java.util.AbstractMap.SimpleEntry;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.CPEN431.Project.CPEN431_2022_Project.RequestReplyProtocolHandle.ReceivedRequest;
import com.google.protobuf.ByteString;
 
import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class KeyValueMessageResponseTask implements Runnable {
  
 
   protected static ConcurrentHashMap<ByteString, Builder> cachedResponses= new ConcurrentHashMap<ByteString, Builder>( );
   protected static ConcurrentLinkedQueue<SimpleEntry<Date,  ByteString>> cachedHeaderIds = new ConcurrentLinkedQueue<SimpleEntry<Date,  ByteString>>();
   protected KeyValueHandle valueHandle ;
   protected RequestReplyProtocolHandle protocolHandle;
   protected MessageHandle msgHandle;
   protected ReceivedRequest request;
   protected EpicdemicServer epicdemicServer;
   protected boolean needShutDown;
   protected static int cachedValueSize =0;
   // add checking Matthew
   protected static int totalDeleted = 0;
   private static Integer pid=null;
   
    public KeyValueMessageResponseTask(KeyValueHandle keyValueHandle,RequestReplyProtocolHandle pHandle,  MessageHandle mHandle,ReceivedRequest r,
    		EpicdemicServer epicdemic)  {
    	epicdemicServer = epicdemic;
        protocolHandle = pHandle;
        msgHandle = mHandle;
        valueHandle = keyValueHandle;
        request = r;
        needShutDown = false;
   
        
    }
    
    public static int getPID() {
    	if ( pid == null) {
    		int result =0;
            String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
            if (processName != null && processName.length() > 0) {
                
                 result =  Integer.parseInt(processName.split("@")[0]);
                
            }

            pid = result;
    	}
    	
        return pid;
    }	 
    
    public static void cleanLegacyCache() {
    	Date now = new Date();
    
    	boolean hasDeletedRecord = false;
    	Builder kvBuilder = null;
    	
    	// add checking Matthew
    	int deletedSize = 0;

    	SimpleEntry<Date,  ByteString> topHeader = cachedHeaderIds.peek(); 
    	while ( topHeader != null  ) {
    		if ( (int)((now.getTime()- topHeader.getKey().getTime())/1000) > 5 ) {
    			kvBuilder = cachedResponses.remove(topHeader.getValue());
    	    	if (kvBuilder != null) {
//     				cachedValueSize -= kvBuilder.build().toByteString().size();
    	    		cachedValueSize -= kvBuilder.getValue().size();
        			cachedHeaderIds.poll();
        			hasDeletedRecord = true;
        			
        			// add checking Matthew
        			deletedSize += kvBuilder.getValue().size();
        			totalDeleted += kvBuilder.getValue().size();
    	    	}
    		}else
    			break;
    		topHeader = cachedHeaderIds.peek(); 
    	}
    	
    	if ( hasDeletedRecord )
    		// add checking Matthew
    		if (deletedSize > 500000) {
    			System.gc();
    			System.out.println("Deleted cache size = " + deletedSize + " : Total deleted = " + totalDeleted);
    			deletedSize = 0;
    		}
    	// System.gc();
    	 
    }
    
    protected static void pushCacheResponse(ByteString messageId, Builder builder) {
    	if (builder != null) {
//        	cachedValueSize += builder.build().toByteString().size();
    		cachedValueSize += builder.getValue().size();
//        	cachedResponses.put(messageId, CompressionUtils.compress(payload.toByteArray()) );
        	cachedResponses.put(messageId, builder);
        	cachedHeaderIds.add(new SimpleEntry<Date,ByteString>(new Date(), messageId));
    	}
    }
    public void run() {
 
             
              
    	 
        Builder responseBuilder = KVResponse.newBuilder();
        Msg repliedMessage = null;
        boolean isSuccess = false;

        boolean isRouted = false;
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
        try{
                if ( msgHandle.isValid( request.message ) )
                {
                	Builder cachedResponse = cachedResponses.get(request.message.getMessageID());
                	if (cachedResponse != null) {
//                    		repliedMessage = msgHandle.getMessage(CompressionUtils.decompress(cachedResponse) ,request.message.getMessageID().toByteArray());
                   		repliedMessage = msgHandle.getMessage(cachedResponse.build().toByteArray() ,request.message.getMessageID().toByteArray());
                	}
                	KVRequest kvRequest = KVRequest.parseFrom(request.message.getPayload().toByteArray());
            		if ( kvRequest.hasClientPort() && kvRequest.hasClientIP() ) {
            			request.requestPortNumber = kvRequest.getClientPort();
            			request.requestIPAddress = InetAddress.getByName(kvRequest.getClientIP());
            		}
            		
                	if ( repliedMessage == null) {
                		if ( cachedValueSize <= App.cacheLimit) {
                			
                			
                			
	                	
	                    	if ( kvRequest.hasCommand())
	                    	{
	                			int command =  kvRequest.getCommand();
	                		    if ( command == RequestCommand.GETPID.toInt() ) {
	                    			responseBuilder.setPid(getPID());
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.GETMEMBERSHIPCOUNT.toInt()) {
	                    			int count = epicdemicServer.getAliveMemberCount();
	                    			responseBuilder.setMembershipCount(count);
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.ISALIVE.toInt()) {
	                    			
	                    			isSuccess = true;
	                    		}else if ( command == RequestCommand.SHUTDOWN.toInt()) {
	                    			needShutDown = true;
	                    			//Seems not good
	                    			System.exit(0);
	                    		}else
	                    		{
	                    			
	                    			int processResult = valueHandle.process(kvRequest,request, responseBuilder);
	                    			if ( processResult ==  1)
	                    				isSuccess = true;
	                    			else if ( processResult == 2)
	                    				isRouted = true;
	                    			
	                    		}
	                    	}else {
	                    		responseBuilder.setErrCode( ReplyErrorCode.UNRECOGN.toInt() );
	                    	}
                		}else {
                			
                			responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
                			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//                	        System.out.println("Overload due to cache.");
//                	        // creating a new object of the class Date  
//                	        java.util.Date date = new java.util.Date();    
//                			currentTime = dateFormat.format(date);
//            				System.out.println(currentTime + " KV Store Used/Total: " + valueHandle.numberOfByteUsed + "/" + App.minKVStoreSize);
//            				System.out.println(currentTime + " Cache Used/Total: " + cachedValueSize + "/" + App.cacheLimit);
//            				System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
//          				System.out.println("KV Store Used/Total: " + valueHandle.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache Used/Total: " + cachedValueSize + "/" + App.cacheLimit + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
                		}
                	}
                	
                		
                }else{
            	
            	//TODO: Error code for invalid checksum?
	                	responseBuilder.setErrCode( ReplyErrorCode.UNRECOGN.toInt()  );
	              }
            
        	if ( !needShutDown && !isRouted) {
        		
        		if ( repliedMessage == null ) {
        			if ( isSuccess ) {
                    	responseBuilder.setErrCode(ReplyErrorCode.SUCCESSFUL.toInt());
                    	
                    }else if (!responseBuilder.hasErrCode()) {
                 
                    	responseBuilder.setErrCode(ReplyErrorCode.INTERNALFAILURE.toInt());
                    }
                    
        		 
        	        repliedMessage = msgHandle.getMessage(responseBuilder.build().toByteArray(),request.message.getMessageID().toByteArray());
                 	if ( responseBuilder.getErrCode() != ReplyErrorCode.OVERLOAD.toInt())
                 		pushCacheResponse(request.message.getMessageID(), responseBuilder);
                 
                   
                     
                    	
        		} 
        		
        		if ( repliedMessage != null )
            		protocolHandle.send(request.requestIPAddress, request.requestPortNumber, repliedMessage);
        		//System.out.println("Sent");
        	}
        }catch (Exception exp) {
        	exp.printStackTrace();
            System.out.println("Exception : " + exp.getMessage());
        }

        	 
       
       
    }
		 
    public void localRun(ReceivedRequest r) {
    	request = r;
    	run();
    }
    
}
