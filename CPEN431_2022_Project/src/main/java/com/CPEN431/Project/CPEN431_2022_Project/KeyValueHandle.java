package com.CPEN431.Project.CPEN431_2022_Project;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;


import com.CPEN431.Project.CPEN431_2022_Project.RequestReplyProtocolHandle.ReceivedRequest;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse.Builder;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.NodeStatus;

public class KeyValueHandle {


	public int memoryLimit;
	public int numberOfByteUsed;
	ConcurrentHashMap<ByteString, CacheObject> cache;
	// creating a new object of the class Date  
	private java.util.Date date = new java.util.Date();    
	protected EpicdemicServer epicdemicServer;
	protected MessageHandle msgHandle;
	protected RequestReplyProtocolHandle replyProtocolHandle;
	private long numberOfPUT = 0;
	private long numberOfGET = 0;
	private long numberOfREM = 0;
	
	public KeyValueHandle(int memLimit, EpicdemicServer epic,RequestReplyProtocolHandle protocolHandle,MessageHandle mHandle) {
		
	 
		//TODO : Load factor will affect the performance, Let turn it when there is a performance hit
		cache = new ConcurrentHashMap<ByteString, CacheObject>();
		memoryLimit = memLimit;
		numberOfByteUsed =0;
		epicdemicServer = epic;
		replyProtocolHandle = protocolHandle;
		msgHandle = mHandle;
	}
	
	protected int getHandleNodeId(KVRequest kvRequest) {
		int result = -1;
		// creating a new object of the class Date  
        java.util.Date date;    
		DateFormat dateF = new SimpleDateFormat("HH:mm:ss SSS", Locale.ENGLISH);
		dateF.setTimeZone(TimeZone.getTimeZone("PST"));
        String currentTime;
		
		if ( kvRequest.hasKey()) {
			
			 
			result = epicdemicServer.getNodeID(Util.hash( kvRequest.getKey()), true);
			 
	        date = new java.util.Date();    
	        currentTime = dateF.format(date);
			Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Key ID: " + Util.hash( kvRequest.getKey()) + " Found ID: " + result + "\n", "");
			
			
		}
		
		
		return result;
	}
	
	public boolean route(int nodeId, KVRequest kvRequest, ReceivedRequest request) {
		boolean result = false;
		NodeStatus nodeStatus = epicdemicServer.getNodeStatus(nodeId);
		if ( nodeStatus != null ) {
			KeyValueRequest.KVRequest.Builder builder = KeyValueRequest.KVRequest.newBuilder();
			
			
			builder.setClientIP(request.requestIPAddress.getHostAddress());
			builder.setClientPort(request.requestPortNumber);
			
			if ( kvRequest.getCommand() == RequestCommand.GET.toInt()) {
				builder.setCommand(RequestCommand.INTERNALGET.toInt());
				builder.setKey(kvRequest.getKey());
			}else if ( kvRequest.getCommand() == RequestCommand.PUT.toInt()) {
				builder.setCommand(RequestCommand.INTERNALPUT.toInt());
				builder.setKey(kvRequest.getKey());
				builder.setValue(kvRequest.getValue());
				builder.setVersion(kvRequest.getVersion());
			}else if ( kvRequest.getCommand() == RequestCommand.REMOVE.toInt()) {
				builder.setCommand(RequestCommand.INTERNALREMOVE.toInt());
				builder.setKey(kvRequest.getKey());
			 
			}

		    Msg msg = msgHandle.getMessage(builder.build().toByteArray(), request.message.getMessageID().toByteArray());
			
			
		  
		   
		    
			try {
				result = replyProtocolHandle.send(InetAddress.getByName(nodeStatus.getIp()), nodeStatus.getPort(), msg);
				
				
			 
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			
		}
		
		return result;
	}
	
	
	public int process(KVRequest kvRequest, ReceivedRequest request,  Builder responseBuilder) {
 
		//0 = fail
		//1 = success
		//2 = route
		int result = 0;
		boolean isSuccess = false;
		if ( kvRequest != null ) {
			
			if ( kvRequest.hasCommand() ) {
				
				int command = kvRequest.getCommand();
				//System.out.println(" Command : " + command + " Memory : " + Runtime.getRuntime().freeMemory());
				
				
    	        // creating a new object of the class Date  
    	        java.util.Date date = new java.util.Date();    
    			Runtime runtime =  Runtime.getRuntime();
    			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss SSS", Locale.ENGLISH);
    			dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
    	        String currentTime = dateFormat.format(date);

				if (  command ==  RequestCommand.PUT.toInt()) {
					int nodeId = getHandleNodeId(kvRequest);
					
					if ( this.epicdemicServer.currentNodeId == nodeId) {
						isSuccess= putOperation(kvRequest, responseBuilder);
						Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " PUT Val: " + kvRequest.getValue() + " Ver: " + kvRequest.getVersion() + "\n", "");
						numberOfPUT++;
					}
					else {
						
						if ( this.route(nodeId, kvRequest,request) ) {
							Util.writeLineToLogFile(currentTime + " Route from " + this.epicdemicServer.currentNodeId + " to " + nodeId + " Mem: " + this.epicdemicServer.getAliveMemberCount() + "\n", "");
							result = 2;
						}
								
						
						 
					  
						
					
					}
						
				}else if ( command == RequestCommand.GET.toInt()) {
					int nodeId = getHandleNodeId(kvRequest);
					if ( this.epicdemicServer.currentNodeId == nodeId) {
						isSuccess= getOperation(kvRequest, responseBuilder);
						Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " GET Val: " + responseBuilder.getValue() + " Ver: " + responseBuilder.getVersion() + "\n", "");
						numberOfGET++;
					}
					else {
						if ( this.route(nodeId, kvRequest,request) ) {
							Util.writeLineToLogFile(currentTime + " Route from " + this.epicdemicServer.currentNodeId + " to " + nodeId + " Mem: " + this.epicdemicServer.getAliveMemberCount() + "\n", "");
							result = 2;
						}
						 
					}
						
					
				}else if ( command == RequestCommand.REMOVE.toInt()) {
					int nodeId = getHandleNodeId(kvRequest);
					if ( this.epicdemicServer.currentNodeId == nodeId) {
						isSuccess= removeOperation(kvRequest, responseBuilder);
						Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " REM Val: " + responseBuilder.getValue() + " Ver: " + responseBuilder.getVersion() + "\n", "");
						numberOfREM++;
					}
					else {
						if ( this.route(nodeId, kvRequest,request) ) {
							Util.writeLineToLogFile(currentTime + " Route from " + this.epicdemicServer.currentNodeId + " to " + nodeId + " Mem: " + this.epicdemicServer.getAliveMemberCount() + "\n", "");
							result = 2;
						}
						
						
						 
					}
						
					
				}else if (  command ==  RequestCommand.INTERNALPUT.toInt()) {
					isSuccess= putOperation(kvRequest, responseBuilder);
					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Int PUT Val: " + kvRequest.getValue() + " Ver: " + kvRequest.getVersion() + "\n", "");
					numberOfPUT++;
				}else if ( command == RequestCommand.INTERNALGET.toInt()) {
					isSuccess= getOperation(kvRequest, responseBuilder);
					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Int GET Val: " + responseBuilder.getValue() + " Ver: " + responseBuilder.getVersion() + "\n", "");
					numberOfGET++;
				}else if ( command == RequestCommand.INTERNALREMOVE.toInt()) {
					isSuccess= removeOperation(kvRequest, responseBuilder);
					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Int REM Val: " + responseBuilder.getValue() + " Ver: " + responseBuilder.getVersion() + "\n", "");
					numberOfREM++;
				}else if (  command == RequestCommand.WIPEOUT.toInt()) {
//					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Com: " + command + " No. of PUT: " + numberOfPUT + " KVS U/T: " + this.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache U/T: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit + " F/U Mem: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()) + "\n", "");
//					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Com: " + command + " No. of GET: " + numberOfGET + " KVS U/T: " + this.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache U/T: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit + " F/U Mem: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()) + "\n", "");
//					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Com: " + command + " No. of REM: " + numberOfREM + " KVS U/T: " + this.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache U/T: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit + " F/U Mem: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()) + "\n", "");
					Util.writeLineToLogFile(currentTime + " Port: " + this.epicdemicServer.currentNodeId + " Com: " + command + " KVS Size: " + this.cache.size() + " KVS U/T: " + this.numberOfByteUsed + "/" + App.minKVStoreSize + " Cache U/T: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit + " F/U Mem: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()) + "\n", "");
					cache.clear();
					System.gc();
					numberOfByteUsed =0;
					isSuccess = true;
				}else {
					responseBuilder.setErrCode(ReplyErrorCode.UNRECOGN.toInt());
				}
				
				
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.UNRECOGN.toInt());
			}
				
			
		
			
			
			
		}
		if ( isSuccess   ) {
			result = 1;
			responseBuilder.setErrCode(ReplyErrorCode.SUCCESSFUL.toInt());
		}else if (!responseBuilder.hasErrCode()   && result != 2 ) {
			responseBuilder.setErrCode(ReplyErrorCode.INTERNALFAILURE.toInt());
		
		}
		
		return result;
	}
	public boolean removeOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		if (isGetOrRemoveRequestValid(kvRequest, responseBuilder) ) {
			
			CacheObject cacheObj = cache.remove(kvRequest.getKey());
			if ( cacheObj != null ) {
				isSuccess = true;
				numberOfByteUsed -= cacheObj.value.size();
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.NONEXISTKEY.toInt());
			}
				
			
			
			
		}

		return isSuccess;
		
	}
	public boolean getOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		if (isGetOrRemoveRequestValid(kvRequest, responseBuilder) ) {
			
			CacheObject cacheObj = cache.get(kvRequest.getKey());
			if ( cacheObj != null ) {
				
			 
					 
						
//				responseBuilder.setValue(ByteString.copyFrom( CompressionUtils.decompress(cacheObj.value) ));
				responseBuilder.setValue(cacheObj.value);
				responseBuilder.setVersion(cacheObj.version);
				isSuccess = true;
			 
	  
		       
			}else {
				responseBuilder.setErrCode(ReplyErrorCode.NONEXISTKEY.toInt());
			}
				
			
			
			
		}

		return isSuccess;
		
	}
	
	public boolean putOperation(KVRequest kvRequest,  Builder responseBuilder) {
		boolean isSuccess = false;
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		String currentTime;

		if (isPutRequestValid(kvRequest, responseBuilder) ) {
			Runtime runtime =  Runtime.getRuntime();
			
			if (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory() >= memoryLimit && this.numberOfByteUsed < App.minKVStoreSize) {
				try {
					if ( putToCache(kvRequest )){
						isSuccess = true;
					}
				} catch (OutOfMemoryError oome) {
					responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
	    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//	    	        System.out.println("Overload due to Out of Memory when PUT.");
//	    	        // creating a new object of the class Date  
//	    	        java.util.Date date = new java.util.Date();    
//	    			currentTime = dateFormat.format(date);
//	    			System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//	    			System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//	    			System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
		        }
			} else if ( this.numberOfByteUsed < App.minKVStoreSize ) {
				responseBuilder.setErrCode(ReplyErrorCode.OVERLOAD.toInt());
    			responseBuilder.setOverloadWaitTime(App.overloadWaitTime);
//    	        System.out.println("Overload! Free Memory below limit but KV Store is still not met.");
//    	        // creating a new object of the class Date  
//    	        java.util.Date date = new java.util.Date();    
//    			currentTime = dateFormat.format(date);
//    			System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//    			System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//    			System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
			}
			else {
				responseBuilder.setErrCode(ReplyErrorCode.OUTOFSPACE.toInt());
		        // creating a new object of the class Date  
//		        java.util.Date date = new java.util.Date();    
//				currentTime = dateFormat.format(date);
//				System.out.println(currentTime + " KV Store Used/Total: " + this.numberOfByteUsed + "/" + App.minKVStoreSize);
//				System.out.println(currentTime + " Cache Used/Total: " + KeyValueMessageResponseTask.cachedValueSize + "/" + App.cacheLimit);
//				System.out.println(currentTime + " Free/Used Memory: " + (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()) + "/" + (runtime.totalMemory() - runtime.freeMemory()));
			}
		}

		return isSuccess;
		
	}
	
	
	public boolean putToCache(KVRequest kvRequest){
		boolean result = false;
		if ( kvRequest != null ) {
			
			int version =0;
			if ( kvRequest.hasVersion())
				version = kvRequest.getVersion();
		 
			
			 
			//TODO : Always get the latest version and ignore old version (Retry failure)?
			CacheObject cacheObj = cache.get(kvRequest.getKey());
			if ( cacheObj != null  ) {
				numberOfByteUsed -= cacheObj.value.size();
				
			}
//			byte[] output = CompressionUtils.compress(kvRequest.getValue().toByteArray());
//			numberOfByteUsed += output.length;
//			cache.put(kvRequest.getKey(), new CacheObject(version,output));
			ByteString output = kvRequest.getValue();
			numberOfByteUsed += output.size();
			cache.put(kvRequest.getKey(), new CacheObject(version, output));
			result = true;
	       
			
		}
		
		return result;
		
	}
	protected boolean isGetOrRemoveRequestValid(KVRequest kvRequest, Builder responseBuilder) {
		boolean result = true;
		
		ReplyErrorCode error = null;
		if ( kvRequest != null ) {
			if (! kvRequest.hasKey() || kvRequest.getKey().size() > 32  )
			{
				 error = ReplyErrorCode.INVALIDKEY;
				
			} 
		}
		
		

		if ( error != null ) {
			responseBuilder.setErrCode(error.toInt());
			result = false;
		}
		
		return result;
	}
	protected boolean isPutRequestValid(KVRequest kvRequest, Builder responseBuilder) {
		boolean result = true;
		
		ReplyErrorCode error = null;
		if ( kvRequest != null ) {
			if (! kvRequest.hasKey() || kvRequest.getKey().size() > 32  )
			{
				 error = ReplyErrorCode.INVALIDKEY;
				
			}else if  ( !kvRequest.hasValue() || kvRequest.getValue().size() > 10000 ){
				error = ReplyErrorCode.INVALIDVAL;
			}
		}
		
		if ( error != null ) {
			responseBuilder.setErrCode(error.toInt());
			result = false;
		}
		
		
		return result;
	}
}
