package com.CPEN431.Project.CPEN431_2022_Project;

public class CacheCleaner extends Thread {
	protected boolean isRunning = true;

	@Override
	public void run() {
        while (isRunning) {
    		// TODO Auto-generated method stub
    		super.run();
        	try {
        		KeyValueMessageResponseTask.cleanLegacyCache();
        		Thread.sleep(App.gcTime);
        	}
        	catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        System.exit(0);
	}
}
