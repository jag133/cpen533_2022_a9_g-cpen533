package com.CPEN431.Project.CPEN431_2022_Project;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.google.protobuf.ByteString;


import ca.NetSysLab.ProtocolBuffers.Message.Msg;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.Builder;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.NodeStatus;


/**
 *Epicdemic server which responsible to fire network status update to a number of nodes regularly
 *Also to trigger a new thread responsible for receive network status update from other nodes
 */ 
public class EpicdemicServer extends Thread {
    
	private volatile boolean running;
	private NodeStatus nodeStatus;
	protected static ConcurrentHashMap<Integer, NodeStatus> nodeStatusMap = new ConcurrentHashMap<Integer, NodeStatus>( );
	private static int epicdemicportStart = 123;
	private static int internalReplyPortStart = 1000;
	private static int epicdemicbuffer = 5000;
    private DatagramSocket epicdemicSocket;
    private EpicdemicReceiver epicdemicReceiveThread;
    public int currentNodeId;
    private NStatus networkStatus;
    private int isAlivePeriod = 30;
    private int sleepTime = 200;
    //private int noOfNodes = 3;
    private int nodeRange = 255;
   

    public EpicdemicServer(int id , NStatus networkStatus) throws IOException {
        running = false;
        
        this.currentNodeId = id;
        this.networkStatus  = networkStatus;
        
        //Build the hash map
        buildNetworkHashMap(this.networkStatus, this.currentNodeId);
        
        //Define socket parameters and a specific port will be used for epicdemic transmission only
        //epicdemicSocket = new DatagramSocket(nodeStatus.getPort() + epicdemicport, InetAddress.getByName(nodeStatus.getIp()));
        InetAddress ip = InetAddress.getByName(nodeStatus.getIp());
        epicdemicSocket = new DatagramSocket(nodeStatus.getPort() + epicdemicportStart, ip);
        epicdemicSocket.setReceiveBufferSize(epicdemicbuffer);
        
        //Define the epicdemic receive thread and start
        epicdemicReceiveThread = new EpicdemicReceiver(networkStatus , epicdemicSocket);
        new Thread(epicdemicReceiveThread).start();
    }

    public int getInternalReplyPort() {
    	return   (nodeStatus.getPort() +internalReplyPortStart);
    }
    
    
    private void buildNetworkHashMap(NStatus networkStatus, int id) {
		
    	//Put the list of nodes to the hash map and locate this node
    	for (NodeStatus node : networkStatus.getNodestatusList()) {
			nodeStatusMap.put(node.getId(), node);
			if (id == node.getId()) {
				NodeStatus.Builder nsb = NStatus.NodeStatus.newBuilder();
				nsb.mergeFrom(node);
				nsb.setLastseen(System.currentTimeMillis());
				nodeStatus = nsb.build();
				System.out.println("Initialize node :" + nodeStatus.getId() + " last seen : " + nodeStatus.getLastseen());
			}
		}		
	}



	/**
     * A function to check whether an input node is alive or not
     * @return whether the input node is alive or not
     * @throws IOException
     */
    public boolean isAlive(int id) {    	
    	
    	// Use the input node information to check the network status of a particular node
    	NodeStatus node = getNodeStatus( id);
    	
    	if (node != null && node.hasLastseen()) {
    		// If the time retrieved within 3 minutes range then it is alive
    		//System.out.println("Node is alive : " + node.getId());
    		Timestamp latestAliveTime = new Timestamp(node.getLastseen());
    		return latestAliveTime.after(new Date(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(isAlivePeriod)));   	
    	} else
    		return false; 	
   	
    }
    
    
    public NodeStatus getNodeStatus(int id) {
    	return nodeStatusMap.get(id);
    }
    
    /**
     * A function to compare the received copy of network status to the local copy and make necessary update
     */
    public void compareAndUpdate(NStatus receiveNS) throws IOException {
    	
    	//loop network status and match with id     	
    	for (NodeStatus node : receiveNS.getNodestatusList()) {
    		//if receive node timestamp is after the original copy then update to local copy
    		//System.out.println("Last seen from node before :" + node.getId() + " " + node.getLastseen());
    		if (node.hasLastseen() && node.getLastseen() > 0) {
    			Timestamp receiveTS = new Timestamp(node.getLastseen());
    			//System.out.println("Last seen from node :" + node.getId() + " " + node.getLastseen());
        		if (receiveTS.after(new Date(nodeStatusMap.get(node.getId()).getLastseen()))) {
        			nodeStatusMap.put(node.getId(), node);
        			//System.out.println("Last seen updated in local node : " + nodeStatus.getId() + " for network node :" + node.getId());
        		}
    		}
  		}		
    	 
    }
    
    public int getAliveMemberCount() {
    	
    	int result = 0;
    	for (NodeStatus node : networkStatus.getNodestatusList()) {
    		 if ( this.isAlive(node.getId()))
    			 result++;
  		}		
    	return result;
    }
    
    
    
    /**
     * Get a non-repeated random number referring to different node and not host node
     * @param ranNumList
     * @return random number
     * @throws IOException
     */
    public int genRandomNum(List<Integer> ranNumList) throws IOException {
    	Random rand = new Random();
    	int rann = rand.nextInt(nodeRange) + 1;
    	
    	//Return the random number if there is no element in the list
    	if (ranNumList.isEmpty() && getNodeID(rann,false) != currentNodeId) {
    		//ranNumList.add(rann);
    		return rann;
    	}
     	else {
     		//if the list has element, then check if the rann number is already in the list
     		Collections.sort(ranNumList);
    		while (Collections.binarySearch(ranNumList, getNodeID(rann,false)) >= 0 || getNodeID(rann,false) == currentNodeId) {
    			//regen the number until is is not equal to any of the number in the list
    			rann = rand.nextInt(nodeRange) + 1;
    		}
    	}
    	return rann;
    }
    public  List<Integer> genRandomNumList() throws IOException {
    	
    	List<Integer> resultList = new ArrayList<Integer>();
    	Random rand = new Random();
    	ArrayList<Integer> nodeList = new ArrayList<Integer>(nodeStatusMap.keySet());
    	
    	do {
    		int rann = rand.nextInt(nodeList.size());
    		int selectedNodeId =nodeList.get(rann); 
    		if ( selectedNodeId != currentNodeId && !resultList.contains(selectedNodeId) )
    			resultList.add(selectedNodeId);
    		
    	}while ( resultList.size() < 3 && resultList.size() <  nodeStatusMap.size()-1);
    	
    	
    	return resultList;
    }
    
    
    /**
     * Build the network status object 
     * @return network status builder object
     * @throws IOException
     */
    private Builder buildLocalNS() throws IOException {
    	
    	NStatus.Builder nsBuilder = NStatus.newBuilder();
    	NodeStatus.Builder nBuilder = NStatus.NodeStatus.newBuilder();
    	
    	//Loop the hash map and load the entry to the network status builder 
    	for (ConcurrentHashMap.Entry<Integer, NodeStatus> entry : nodeStatusMap.entrySet()) {
    		//nBuilder.mergeFrom(entry.getValue());
    		nBuilder.setId(entry.getValue().getId()).setIp(entry.getValue().getIp())
    			.setPort(entry.getValue().getPort()).setLastseen(entry.getValue().getLastseen());
//    		System.out.println("build local NS :" + entry.getValue().getId() + " " + entry.getValue().getLastseen() + " for node " + currentNodeId);
    		nsBuilder.addNodestatus(nBuilder.build());
    	}
    	
		return nsBuilder;
	}



    
    /**
     * Sends current image of network status to a new node at a set interval while incrementing its own heartbeat at every interval
     */
    @Override
    public void run() {
        running = true;

        byte[] data = new byte[epicdemicbuffer];
        DatagramPacket packet = new DatagramPacket(data, epicdemicbuffer);
        
        while(running) {
            try {
                
            	//update the network status of the local node
            	NodeStatus.Builder ns = NodeStatus.newBuilder();
            	nodeStatusMap.put(currentNodeId, ns.mergeFrom(nodeStatusMap.get(currentNodeId)).setLastseen(System.currentTimeMillis()).build());
            	
            	List<Integer> ranNumList = genRandomNumList();
            	
            	//pick at most 3 random node and fire network status update
            	for (int i = 0; i<ranNumList.size(); i++) {
            		
            		
            		//Look up node range from the randomID
            		int nodeID = ranNumList.get(i);
            		//Check whether the node is alive or not
            		//Or if it is an initial load
//            		if (isAlive(nodeID) || nodeStatusMap.get(nodeID).getLastseen() == 0) {
            			//Prepare for the packet info and send
            			packet.setAddress(InetAddress.getByName(nodeStatusMap.get(nodeID).ge);
                        packet.setPort(nodeStatusMap.get(nodeID).getPort() + epicdemicportStart);
                        packet.setLength(nodeStatusMap.size());
                        
                        NStatus.Builder localNS = buildLocalNS();
                        packet.setData(localNS.build().toByteArray());
                        epicdemicSocket.send(packet);
                        
//                        System.out.println("network status sent to node [" + packet.getAddress().getHostAddress()
//                                + ":" + packet.getPort() + "]" + " " + i + " " + nodeID + " "
//                        		+ nodeStatusMap.get(currentNodeId).getLastseen());
//            		}
            	}
            	System.out.println("Members Count : " + getAliveMemberCount());
                //sleep for 500ms 
                sleep(sleepTime);
            } catch (IOException | InterruptedException e) {
            	e.printStackTrace();
                System.out.println("Exception : " + e.getMessage());
            }
        }

        epicdemicReceiveThread.stop();

        System.out.println("network status sending stopped");
    }

    
    
    /**
     * Retrieve the node ID from a given range of number from 0 - 255
     * @param randomID
     * @return node ID
     */
	public int getNodeID(int randomID,boolean filterIsAlive )   {
		//Convert the node hashmap to list
		List<Integer> result = new ArrayList<Integer>();
		for (NodeStatus node : networkStatus.getNodestatusList()) {
			
			
   		 if ( !filterIsAlive ||  this.isAlive(node.getId()) )
   			result.add(node.getId());
 		}		
		//Sort the list and perform binary search on it to get the result node index
		Collections.sort(result);
		int nodeIndex = Collections.binarySearch(result, randomID);
		
		//Get the nodeID based on the index
		//if the node index >= 0, that means there is a perfect match of the node ID, just return the nodeID stored in the index
		//if the node index <0, that means no match found, the index will point to the expected position using negative number -1
		if (nodeIndex < 0)
			nodeIndex = (nodeIndex*-1)-1;
		if (nodeIndex == result.size())
			nodeIndex = 0;
		if (nodeIndex >= result.size())
			return -1;
		return result.get(nodeIndex);
	
//		if (nodeIndex >= 0)
//			return result.get(nodeIndex);
//		else
//			return result.get((nodeIndex*-1)-1);
	}



	/**
     * Starts the epicdemic server thread
     */
    @Override
    public void start() {
        if (!running) {
            try {
                super.start();
            } catch (IllegalThreadStateException ignored) {
            }
        }
    }

    public void exit() {
        running = false;
        epicdemicSocket.close();
    }

    
    /**
     * A runnable class responsible for the network status receive and update of the epicdemic protocol
     */
    private class EpicdemicReceiver implements Runnable {
        private volatile boolean running;

        private DatagramSocket ds;
        private NStatus ns;

        public EpicdemicReceiver (NStatus networkStatus, DatagramSocket ds) {
            this.ns = networkStatus;
            this.ds = ds;

            running = false;
        }

        /**
         * Stops the receive thread
         */
        public void stop() {
            running = false;
        }

        /**
         * Receives epicdemic status update from other nodes and updates its own list of network status
         */
        @Override
        public void run() {
            running = true;

            System.out.println("start epicdemic receive");

            //Assign 2000 bytes to the buffer
            byte[] data = new byte[epicdemicbuffer];
            DatagramPacket packet = new DatagramPacket(data, epicdemicbuffer);
            
            while (running) {
                try {
                    // receive packet from the epicdemic socket
                	packet.setLength(epicdemicbuffer);
                    ds.receive(packet);
      
                    //System.out.println("network status received from node [" + packet.getAddress().getHostAddress()
                    //        + ":" + packet.getPort() + "]");
                   
                    //Capture the response data and parse it to the message object
					byte[] resdata = Arrays.copyOfRange(packet.getData(), 0, packet.getLength());				
					//Msg resmsg = Msg.parseFrom(ByteString.copyFrom(resdata));
					
					//Retrieve the payload and store into networkStatus object
					NStatus receiveNS = NStatus.parseFrom(resdata);
					//System.out.println("Get network 1 :" + receiveNS.getNodestatus(0).getLastseen());
					//System.out.println("Get network 2 :" + receiveNS.getNodestatus(1).getLastseen());
					//System.out.println("Get network 3 :" + receiveNS.getNodestatus(2).getLastseen());
					
					//Compare the received network status and update to local copy
					compareAndUpdate(receiveNS);                 
                    
                } catch (IOException e) {
                	e.printStackTrace();
                    System.out.println("Exception : " + e.getMessage());
                }
            }

            System.out.println("stop epicdemic receive");
        }
    }
}
