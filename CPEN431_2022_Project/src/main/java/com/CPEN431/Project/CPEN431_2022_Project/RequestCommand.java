package com.CPEN431.Project.CPEN431_2022_Project;

public enum RequestCommand {
	
	PUT(1),GET(2),REMOVE(3),SHUTDOWN(4),WIPEOUT(5),ISALIVE(6),GETPID(7),GETMEMBERSHIPCOUNT(8),INTERNALPUT(21),INTERNALGET(22),INTERNALREMOVE(23),INVALIDCOMMAND(100);
	private final int code;

	private RequestCommand(int code) {
	        this.code = code;
	    }

    public int toInt() {
        return code;
    }
	
}
