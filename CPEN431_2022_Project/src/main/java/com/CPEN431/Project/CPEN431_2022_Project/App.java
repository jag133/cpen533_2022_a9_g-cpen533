package com.CPEN431.Project.CPEN431_2022_Project;

import java.nio.ByteBuffer;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus;

public class App {
	public static int memoryLimit = 4 * 1024 * 1024;
	public static int cacheLimit = 4 * 1024 *1024;
	public static int minKVStoreSize = 47 * 1024 * 1024;
	public static int overloadWaitTime = 1000;
	public static int numberOfThread = 8;
	public static int numberOfTasks = 2000;
	public static int gcTime = 2000;
	 
	protected static int GetArgumentsOrDefaults(String[] args, int index, int defaultVal) {
		int val = defaultVal;
		if ( args.length > index ) {
			val = Integer.parseInt(args[index]);
		}
		return val;
	}
	protected static String GetArgumentStringOrDefaults(String[] args, int index, String defaultVal) {
		String val = defaultVal;
		if ( args.length > index ) {
			val =  args[index] ;
		}
	 
		return val;
	}
	public static void main(String[] args) {
//    	NetworkStatusWrapper networkStatusWrapper;
//		networkStatusWrapper = new NetworkStatusWrapper(args[0]);
    	
		// TODO Auto-generated method stub
		int serverId =  GetArgumentsOrDefaults(args,0,1);
    	String filePath = GetArgumentStringOrDefaults(args,1,"C:\\temp\\server.file");
		
		
		memoryLimit = GetArgumentsOrDefaults(args,2,3) * 1024 * 1024;
		cacheLimit =   GetArgumentsOrDefaults(args,3,3)   * 1024 * 1024;
		
		minKVStoreSize =  GetArgumentsOrDefaults(args,4,54) * 1024 * 1024;
		overloadWaitTime =  GetArgumentsOrDefaults(args,5,1500)  ;
		numberOfThread = GetArgumentsOrDefaults(args,6,5)  ;
		numberOfTasks =  GetArgumentsOrDefaults(args,7,2000)  ;
		gcTime =  GetArgumentsOrDefaults(args,8,2200)  ;
    	
    	
		 
		System.out.println("Memory Limit: " + memoryLimit);
		System.out.println("Cache Limit: " + cacheLimit);
		System.out.println("Min KV Store Size: " + minKVStoreSize);
		System.out.println("Overload Wait Time: " + overloadWaitTime);
		System.out.println("Number of Threads: " + numberOfThread);
		System.out.println("Number of Tasks: " + numberOfTasks);
		System.out.println("GC Time Interval: " + gcTime);
		 
//	    ByteBuffer bb = ByteBuffer.allocate(8); 
//	    for (int i = 0;i < 999;i++) {
//	    	double temp = Math.random();
//		    bb.putDouble(i); 
////		    System.out.println("Hash of " + i + ": " + Util.hash(ByteString.copyFrom(bb.array())) + "\n");
//		    System.out.println("Hash of " + i + ": " + Util.hash(ByteString.copyFrom(bb.array())) + "\n");
//		    bb.clear();
//	    }
    	
    	try {
    		 NStatus ns = Util.parseNodeFile(filePath);
        	
    		 new ServerThread(serverId,  ns, memoryLimit).run();
    		    
    	}catch (Exception exp) {
    		 System.out.println("error in main" + exp.getMessage());
    	}
		
		
	}

}
