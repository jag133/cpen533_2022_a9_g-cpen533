package com.CPEN431.Project.CPEN431_2022_Project;

import com.google.protobuf.ByteString;

public class CacheObject {

	public CacheObject(int ver, ByteString val) {
		value = val;
		version = ver;
	}
	
	public ByteString value;
	public int version;
}
