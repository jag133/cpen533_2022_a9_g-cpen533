package com.CPEN431.Project.CPEN431_2022_Project;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus;

public class Util {
    protected static final String ALGORITHM_HASH = "MD5";
    protected static final String DELIMITER_NODE = ":";
    protected static final String IGNORED_NODE = "#";
//    protected static final BigInteger HASH_RING_LIMIT = new BigInteger("256");
    protected static final int HASH_RING_LIMIT = 256;
	protected static String logFilePath = "loading.log";
	protected static boolean firstWrite = true;
	protected static boolean debugMode = false;

	static NStatus parseNodeFile(String pathToText) {
        ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.Builder nodeListBuilder = NStatus.newBuilder();
        ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.NodeStatus.Builder nodeBuilder = NStatus.NodeStatus.newBuilder();
        String[] parts;
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(new File(pathToText)));
			
			String line;
			while((line = reader.readLine()) != null) {
	            if (line.contains(IGNORED_NODE))
	                continue;
	            if (!line.contains(DELIMITER_NODE))
	                continue;
				parts = line.trim().split(DELIMITER_NODE);
				nodeBuilder.setId(Integer.parseInt(parts[0]));
				nodeBuilder.setIp(parts[1]);
				nodeBuilder.setPort(Integer.parseInt(parts[2]));
				nodeListBuilder.addNodestatus(nodeBuilder.build());
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return nodeListBuilder.build();
	}

	// logPath is optional.
	public static void writeLineToLogFile(String line, String logPath) {
	    try {
	    	if (debugMode == true) {
		    	boolean append = true;

		    	if (logPath != "") {
		    		logFilePath = logPath;
		    	}
		    	if (firstWrite) {
		    		firstWrite = false;
		    		append = false;
		    	}
		    	BufferedWriter writer = new BufferedWriter(new FileWriter(logFilePath, append));
		    	writer.write(line);
		    	writer.close();
	    	}
	    } catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
	        e.printStackTrace();
	    }
	}

    public static int hash(ByteString key) {
    	int returnValue;
//        MessageDigest md;
//        
//        try {
//            md = MessageDigest.getInstance(ALGORITHM_HASH);
//            md.update(key.toByteArray());
//        } catch (NoSuchAlgorithmException e) {
//			throw new RuntimeException(e);
//        }
//		return (new BigInteger(md.digest())).mod(HASH_RING_LIMIT).intValue();
    	returnValue = key.hashCode() % HASH_RING_LIMIT;
    	if (returnValue < 0)
    		returnValue = 0 - returnValue;
		return returnValue;
    }
}
