package com.CPEN431.Project.CPEN431_2022_Project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus;
import ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus.NodeStatus;

public class NetworkStatusWrapper {
	private NStatus networkStatus;
    private static final int DEAD_AFTER = 5 * 60 * 1000;
			
	public NetworkStatusWrapper(String nodeFilePath) {
		super();
		this.networkStatus = Util.parseNodeFile(nodeFilePath);
	}

	public void printNodeStatus() {
		for (NodeStatus node : this.networkStatus.getNodestatusList()) {
			System.out.println("Node ID: " + node.getId() + " Node IP: " + node.getIp() + " Node Port: " + node.getPort() + " Node Last Seen: " + node.getLastseen());
		}
    }

	public NodeStatus getNodeStatusByID(int id) {
		NodeStatus returnNodeStatus = null;
		
		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (node.getId() == id) {
				returnNodeStatus = node;
			}
		}
		return returnNodeStatus;
    }

	public NodeStatus getNodeStatusByIndex(int index) {
		NodeStatus returnNodeStatus = null;
		int i = 0;
		
		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (i == index) {
				returnNodeStatus = node;
			}
			i++;
		}
		return returnNodeStatus;
    }

	public NodeStatus getNodeStatusByIPPort(String ip, int port) {
		NodeStatus returnNodeStatus = null;
		
		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (node.getIp() == ip && node.getPort() == port) {
				returnNodeStatus = node;
			}
		}
		return returnNodeStatus;
    }

	// Sorting is done when adding a new node status.
	public boolean addNodeStatus(int id, String ip, int port, long lastSeen) {
		boolean result = false;
		
        NStatus.Builder nodeListBuilder = NStatus.newBuilder();
        NodeStatus.Builder nodeBuilder = NStatus.NodeStatus.newBuilder();
    	
    	nodeBuilder.setId(id).setIp(ip).setPort(port).setLastseen(lastSeen);

		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (node.getId() < id) {
				nodeListBuilder.addNodestatus(node);
			}
			else if (node.getId() >= id) {
				if (result == false) {
					result = true;
			        nodeListBuilder.addNodestatus(nodeBuilder.build());
				}
				nodeListBuilder.addNodestatus(node);
			}
		}
		if (result == false) {
			result = true;
	        nodeListBuilder.addNodestatus(nodeBuilder.build());
		}
		this.networkStatus = nodeListBuilder.build();
		return result;
	}

	// Return false when id is not found.
	public boolean updateNodeStatusByID(int byID, int id, String ip, int port, long lastSeen) {
		boolean result = false;
		
        NStatus.Builder nodeListBuilder = NStatus.newBuilder();
        NodeStatus.Builder nodeBuilder = NStatus.NodeStatus.newBuilder();
    	
    	nodeBuilder.setId(id).setIp(ip).setPort(port).setLastseen(lastSeen);

		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (node.getId() != id) {
				nodeListBuilder.addNodestatus(node);
			}
			else {
		        nodeListBuilder.addNodestatus(nodeBuilder.build());
				result = true;
			}
		}
		this.networkStatus = nodeListBuilder.build();
		return result;
	}

	// Caution!!!! No sorting if updating by index. Return false when index is not found.
	public boolean updateNodeStatusByIndex(int byIndex, int id, String ip, int port, long lastSeen) {
		boolean result = false;
		int i = 0;
		
        NStatus.Builder nodeListBuilder = NStatus.newBuilder();
        NodeStatus.Builder nodeBuilder = NStatus.NodeStatus.newBuilder();
    	
    	nodeBuilder.setId(id).setIp(ip).setPort(port).setLastseen(lastSeen);

		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (i != byIndex) {
				nodeListBuilder.addNodestatus(node);
			}
			else {
		        nodeListBuilder.addNodestatus(nodeBuilder.build());
				result = true;
			}
			i++;
		}
		this.networkStatus = nodeListBuilder.build();
		return result;
	}

	public ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus getNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(ca.NetSysLab.ProtocolBuffers.NetworkStatus.NStatus networkStatus) {
		this.networkStatus = networkStatus;
	}

	public boolean removeNodeStatus(int id) {
		boolean result = false;
        NStatus.Builder nodeListBuilder = NStatus.newBuilder();
    	
		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
			if (node.getId() != id) {
				nodeListBuilder.addNodestatus(node);
			}
			else {
				result = true;
			}
		}
		this.networkStatus = nodeListBuilder.build();
		return result;
    }

	public NodeStatus getNodeStatusForKey(ByteString key) {
		NodeStatus returnNodeStatus = null;
		int keyHash = Util.hash(key);
		Date lastSeenDateTime;
	    // creating a new object of the class Date  
	    java.util.Date date = new java.util.Date();    
		
		DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("PST"));
		List<NodeStatus> nodeStatus = this.networkStatus.getNodestatusList();
		for (NodeStatus node : nodeStatus) {
		    Calendar cal = Calendar.getInstance();
		    cal.setTimeInMillis(node.getLastseen());
			lastSeenDateTime = cal.getTime();
		    long diffInMillies = Math.abs(date.getTime() - lastSeenDateTime.getTime());
//		    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		    if (diffInMillies < DEAD_AFTER && node.getId() > keyHash) {
				returnNodeStatus = node;
				break;
			}
		}
		if (returnNodeStatus == null ) {
			returnNodeStatus = nodeStatus.get(0);
		}
		return returnNodeStatus;
	}
	
}
