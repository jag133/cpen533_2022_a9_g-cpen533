package com.CPEN431.Project.CPEN431_2022_Project;

public enum ReplyErrorCode {
	
	SUCCESSFUL(0),NONEXISTKEY(1),OUTOFSPACE(2),OVERLOAD(3),INTERNALFAILURE(4),UNRECOGN(5), INVALIDKEY(6), INVALIDVAL(7); 
	private final int code;

	private ReplyErrorCode(int code) {
        this.code = code;
    }

	public int toInt() {
	    return code;
	}
}
